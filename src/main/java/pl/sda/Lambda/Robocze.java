package pl.sda.Lambda;

public class Robocze {
    public static void main(String[] args) {
        Instrument instrument = new Instrument() {
            @Override
            public void dzwiek() {
                System.out.println("Dzwiek anonimowy");
            }
        };
        instrument.dzwiek();


        Instrument instrument1 = ()-> System.out.println("Dzwiek dzieki wyrazeniu Lambda");
        instrument1.dzwiek();


        instrument2 instrument2 = scale -> System.out.println(scale);

        instrument2.dajDzwiek("a-minor");
    }
}
