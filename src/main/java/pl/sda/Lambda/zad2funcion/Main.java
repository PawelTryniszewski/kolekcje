package pl.sda.Lambda.zad2funcion;

import pl.sda.Lambda.zad1predicate.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

/**
 * FUNCTION
 * Rozgrzewka: napisz funkcję, która przyjmuje listę intów i zwraca listę z samymi liczbami parzystymi
 * 5. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę z samymi kobietami.
 * 6. Napisz funkcję, która przyjmuje Person i zwraca Student (stwórz taką klasę. Niech posiada student id), jeśli osoba
 * ma mniej niż 18 lat. Student id wygeneruj losowo.
 * 7. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę studentów, korzystając z funkcji z poprzedniego
 * zadania.
 */

public class Main {

    public static void main(String[] args) {

        Person adam = new Person("Adam", "kowalski", 11, true);
        Person adam1 = new Person("Pawel", "kowalski", 12, true);
        Person adam2 = new Person("Jacek", "kowalski", 14, false);
        Person adam3 = new Person("Placek", "kowalski", 20, true);


        Function<List<Integer>, List<Integer>> toEvenNumberList = x -> {
            List<Integer> evenNumbers = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
            for (int number : x) {
                if (number % 2 == 0) evenNumbers.add(number);
            }
            return evenNumbers;
        };
        List<Person> listaAdamow = new ArrayList<>(Arrays.asList(adam, adam1, adam2, adam3));
        //Napisz funkcję, która przyjmuje listę <Person> i zwraca listę z samymi kobietami.
        Function<List<Person>, List<Person>> functiongetOnlyWomen = x -> {
            List<Person> people = new ArrayList<>();
            for (Person person : x) {
                if (person.isMale()) people.add(person);
            }
            return people;
        };
        System.out.println(functiongetOnlyWomen.apply(listaAdamow));


        //Napisz funkcję, która przyjmuje Person i zwraca Student (stwórz taką klasę. Niech posiada student id), jeśli osoba
        // * ma mniej niż 18 lat. Student id wygeneruj losowo.
        Function<Person, Student> personStudentFunction = x -> {
            Random r = new Random();
            if (x.getAge()<18){

            Student student = new Student(r.nextInt(1000), x.getFirstName(), x.getLastName(), x.getAge());


            return student;

            }
            else return null;

        };
        System.out.println(personStudentFunction.apply(adam1));

        //Napisz funkcję, która przyjmuje listę <Person> i zwraca listę studentów, korzystając z funkcji z poprzedniego
        // * zadania.
        Function<List<Person>,List<Student>> listListFunction = x->{
            List<Student>listaStudentow = new ArrayList<>();
            for (Person person:x){
                Student student = personStudentFunction.apply(person);
                if (student!=null) listaStudentow.add(student);
            }
            return listaStudentow;
        };
        System.out.println(listListFunction.apply(listaAdamow));


    }
}
