package pl.sda.Lambda.zad1predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * PREDICATE
 * 1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.
 * 2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest String zawierający słowo "gotchya".
 * 3. Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Napisz:
 * 	a) funkcję, która zwraca true, jeśli Person jest mężczyzną
 * 	b) funkcję, która zwraca true, jeśli Person ma na imię "Jacek"
 * 	c) funkcję, która zwraca true, jeśli jest osoba jest pełnoletnia.
 * 4.  Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna, który ma na imię jacek.
 */

public class Main {

    public static void main(String[] args) {
        Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.toLowerCase().contains("a");
            }
        };

        Predicate<String> predicate1 = s -> s.toLowerCase().contains("a");

        System.out.println(predicate.test("bsjad asdasd"));


        Predicate<Integer> liczba = integer -> {
            if (integer%2==0){

            return true;
            }
            else return false;
        };
        System.out.println(liczba.test(5));

        List<String> list = new ArrayList<>();
        list.add("gotchya");
        list.add("dsff");
        list.add("dsf");
        list.add("sdfa");

        Predicate<List<String>> solwo = s -> {
            for (String a:s){

            if (a.contains("gotchya"))return true;

            }
            return false;
        };
        System.out.println(solwo.test(list));

        Person adam = new Person("Adam","kowalski",21,true);
        Person adam1 = new Person("Pawel","kowalski",21,true);
        Person adam2 = new Person("Jacek","kowalski",21,true);
        Person adam3 = new Person("Placek","kowalski",21,true);

        Predicate<Person> personPredicate = person -> person.isMale;

        Predicate<Person> personPredicate1 = person -> person.getFirstName().equals("Jacek");

        //funkcję, która zwraca true, jeśli jest osoba jest pełnoletnia.
        Predicate<Person> personPredicate2 = person -> person.getAge()>18;

        List<Person> listaPersonow = new ArrayList<>(Arrays.asList(adam,adam1,adam2,adam3));

        // Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna, który ma na imię jacek.

        Predicate<List<Person>> personPredicate3 = lista -> {
            boolean dorosly=false;
            for (Person p :lista){
                if (p.getAge()>18&&p.getFirstName().equals("Jacek")){
                    dorosly=true;
                    break;
                }

            }
            return dorosly;
        };
        System.out.println(personPredicate3.test(listaPersonow));

    }
}
