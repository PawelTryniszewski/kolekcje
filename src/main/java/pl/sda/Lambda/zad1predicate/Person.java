package pl.sda.Lambda.zad1predicate;

import pl.sda.Listy.zad6.Student;

import java.util.Comparator;

public class Person implements Comparable<Person> {
    String firstName;
    String lastName;
    int age;

    public String getFirstName() {
        return firstName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isMale=" + isMale +
                '}';
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return isMale;
    }

    boolean isMale;

    public Person(String firstName, String lastName, int age, boolean isMale) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isMale = isMale;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public void setAge(int age) {

        this.age = age;
    }

    @Override
    public int compareTo(Person o) {
        if (this.getAge()>o.getAge())return 1;
        else if (this.getAge()<o.getAge())return -1;
        else return 0;
    }
}
