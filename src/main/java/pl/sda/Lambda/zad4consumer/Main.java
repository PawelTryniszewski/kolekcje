package pl.sda.Lambda.zad4consumer;

import pl.sda.Lambda.zad1predicate.Person;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {

        Person adam = new Person("Adam", "kowalski", 11, true);
        Person adam1 = new Person("Pawel", "kowalski", 12, true);
        Person adam2 = new Person("Jacek", "kowalski", 14, false);
        Person adam3 = new Person("Placek", "kowalski", 20, true);

        Consumer<Person> printFirstName = x-> System.out.println(x.getFirstName()+""+x.getAge());
        printFirstName.accept(adam);
        printFirstName.accept(adam1);

        Supplier<Person> supplier = ()->{
            Random r =new Random();
            return new Person("adam","adamski",r.nextInt(100),true);
        };
    }
}
