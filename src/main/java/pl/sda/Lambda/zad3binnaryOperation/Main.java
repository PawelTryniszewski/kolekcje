package pl.sda.Lambda.zad3binnaryOperation;

import pl.sda.Lambda.zad1predicate.Person;

import java.util.*;
import java.util.function.BinaryOperator;

/**
 * BINARY OPERATOR
 * 8. Napisz funkcję, która przyjmuje dwie Person i zwraca osobę starszą.
 * 9. Napisz funkcję, która przyjmuje dwie listy <Person> i zwraca listę <Person>, w której średnia wieku jest większa.
 * 10. Napisz funkcję, która przyjmuje dwie listy <Person> i zwraca nową listę z samymi mężczyznami z obydwóch list.
 * Obiekty nie powinny się powtarzać (tj. nie może być w liście wynikowej dwóch osób o tych samych polach).
 */

public class Main {
    public static void main(String[] args) {
        Person adam = new Person("Adam", "kowalski", 11, true);
        Person adam1 = new Person("Pawel", "kowalski", 12, true);
        Person adam2 = new Person("Jacek", "kowalski", 14, false);
        Person adam3 = new Person("Placek", "kowalski", 20, true);
        List<Person> peopleList =new ArrayList<>(Arrays.asList(adam,adam2,adam3)) ;
        List<Person> peopleList2 =new ArrayList<>(Arrays.asList(adam1,adam2,adam3)) ;

        BinaryOperator<Person> personBinaryOperator = (osoba,osoba1)->{
            if (osoba.getAge()>osoba1.getAge())return osoba; else return osoba1;

        };

        System.out.println();
        System.out.println(personBinaryOperator.apply(adam,adam1));

        //Napisz funkcję, która przyjmuje dwie listy <Person> i zwraca listę <Person>, w której średnia wieku jest większa.

        BinaryOperator<List<Person>> listBinaryOperator = (a,b)->{
            int sumaA=0;
            int sumaB=0;
            for (Person p :a){
                sumaA+=p.getAge();
            }
            for (Person p : b){
                sumaB+=p.getAge();
            }
            double sredniaA=sumaA/a.size();
            double sredniaB=sumaB/b.size();

            if (sredniaA>sredniaB) return a; else return b;


        };
        System.out.println(listBinaryOperator.apply(peopleList,peopleList2));

        // Napisz funkcję, która przyjmuje dwie listy <Person> i zwraca nową listę z samymi mężczyznami z obydwóch list.
        // * Obiekty nie powinny się powtarzać (tj. nie może być w liście wynikowej dwóch osób o tych samych polach).

        BinaryOperator<List<Person>> listBinaryOperator1 = (a,b)->{
            List<Person> samiMezczyzni = new ArrayList<>();
            for (Person p:a){
                if (p.isMale()){
                    samiMezczyzni.add(p);
                }
            }
            for (Person p:b){
                if (p.isMale()){
                    samiMezczyzni.add(p);
                }
            }
            return samiMezczyzni;
        };
        Set<Person> samiM = new HashSet<>((listBinaryOperator1.apply(peopleList,peopleList2)));


    }
}
