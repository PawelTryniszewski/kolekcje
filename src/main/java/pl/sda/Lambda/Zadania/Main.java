package pl.sda.Lambda.Zadania;

import com.sun.org.apache.xerces.internal.xs.datatypes.ObjectList;
import pl.sda.Lambda.zad1predicate.Person;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

public class Main implements MojConsummer,MojIntrFunkcyjny{
    public static void main(String[] args) {
        Person pablo = new Person("Pablo","Escobar",28,true);
        Person pablo1 = new Person("Jerry","Mouse",28,true);
        Person pablo2 = new Person("Klark","Kent",150,true);
        Person pablo3 = new Person("Bruce","Wayn",33,false);
        Person pablo4 = new Person("Tom","Cat",27,true);
        Programmer joda = new Programmer(pablo,new ArrayList<String>(Arrays.asList("Polski","Angielski","Hiszpanski","Klińgonski","Java","Phyton")));
        Programmer jerry = new Programmer(pablo1,new ArrayList<String>(Arrays.asList("C++","Angielski","Klińgonski","Java","Phyton")));
        Programmer tom = new Programmer(pablo2,new ArrayList<String>(Arrays.asList("C#","Polski","Angielski","Hiszpanski","Klińgonski","Java","Phyton")));
        Programmer batman = new Programmer(pablo3,new ArrayList<String>(Arrays.asList("Pascal","C++","Java","Phyton")));
        Programmer ssMan = new Programmer(pablo4,new ArrayList<String>(Arrays.asList("C++","C#","C","Pascal","Java","Phyton")));

        List<Programmer> listaPajacow = new ArrayList<>(Arrays.asList(joda,jerry,tom,batman,ssMan));

//a) Stwórz funkcję, która przyjmuje listę <Programmer> i mapuje ją na listę <Person>
        Function<List<Programmer>,List<Person>> listListFunction= x->{
            List<Person> listaOsob1 = new ArrayList<>();
            for (Programmer p:x){
                Person person = new Person(p.getPerson().getFirstName(),p.getPerson().getLastName(),p.getPerson().getAge(),
                        p.getPerson().isMale());listaOsob1.add(person);
            }
            return listaOsob1;
        };
        System.out.println(listListFunction.apply(listaPajacow));
//b) stwórz funkcję, która mapuje listę <Programmer> na listę języków, czyli zwraca listę języków
//  znanych przez programistów w liście źródłowej.
        Function<List<Programmer>,Set<String>> listListFunction1= x->{
            Set<String> listaWszystkichJezykow = new HashSet<>();
            int i=0;
            for (Programmer p:x){
                listaWszystkichJezykow.addAll(p.getListaJezykow());
            }

            return listaWszystkichJezykow;
        };
        System.out.println(listListFunction1.apply(listaPajacow));
//c) stwórz funkcję, która wyświetla, ile w liście programistów jest kobiet.
        Consumer<List<Programmer>> consumer = x->{
            int counter=0;
            for (Programmer p:x){
                if (!p.getPerson().isMale()){
                    counter++;
                }
            }
            System.out.printf("Wsrod programistow znajduje się %d kobiet",counter);
        };
        consumer.accept(listaPajacow);
//c) stwórz funkcję, która zwraca listę programistów znających Javę.
        Consumer<List<Programmer>> ileZnaJave=x->{
            int counter=0;
            for (Programmer p:x){
                if(p.getListaJezykow().contains("Java")){
                    counter++;
                }
            }
            System.out.printf("Liczba programistow(K I M) znajacych Jave: %d",counter);
        };
        System.out.println();
        ileZnaJave.accept(listaPajacow);


//d) stwórz funkcję, która wyświetla, ile programistów znających Javę jest pełnoletnich.
        Consumer<List<Programmer>> ilePelnoletnichZnaJave=x->{
            int counter=0;
            for (Programmer p:x){
                if (p.getPerson().getAge()>18&&p.getListaJezykow().contains("Java")){
                    counter++;
                }
            }
            System.out.printf("Liczba pelnoletnich programistow znajacych Jave: %d",counter);
        };
        System.out.println();
        ilePelnoletnichZnaJave.accept(listaPajacow);

//e) stwórz funkcję, która wyświetla, ile programistek zna Javę

        Consumer<List<Programmer>> ileKobietZnaJave = x->{
            int counter=0;
            for (Programmer p:x){
                if (!p.getPerson().isMale()&&p.getListaJezykow().contains("Java")){
                    counter++;
                }
            }
            System.out.printf("Wsrod programistow %d kobiet zna Jave",counter);
        };
        System.out.println();
        ileKobietZnaJave.accept(listaPajacow);

//12*. Napisz funkcję, która przyjmuje tablicę [Person] i sortuje ją według wieku osób. Użyj do tego komparatora.

        Person[] tabOsob = new Person[]{pablo,pablo1,pablo2,pablo3,pablo4};

        Consumer<Person[]> sortujeWgWieku = x->{
            Arrays.sort(tabOsob);
            System.out.println(Arrays.toString(tabOsob));

        };
        System.out.println();
        sortujeWgWieku.accept(tabOsob);

        // @Override
        //    public int compareTo(Person o) {
        //        if (this.getAge()>o.getAge())return 1;
        //        else if (this.getAge()<o.getAge())return -1;
        //        else return 0;
        //    }




        //13**. Napisz własny interfejs funkcyjny, który przyjmuje 3 listy i zwraca jedną listę. Zaimplementuj go
        // według własnego uznania. Możesz użyć klas, które stworzyłeś we wcześniejszych zadaniach.
        MojIntrFunkcyjny<List<Person>,List<Programmer>,List<Person>,List<Person>> mojIntrFunkcyjny =(a,b,c)->{
            List<Person> listaWszystkiego = new ArrayList<>();
            for (Person p :a){
                listaWszystkiego.add(p);
            }
            for (Programmer p :b){
                Person person = new Person(p.getPerson().getFirstName(),p.getPerson().getLastName(),p.getPerson().getAge(),
                        p.getPerson().isMale());listaWszystkiego.add(person);
            }
            for (Person p:c){
                listaWszystkiego.add(p);
            }

            return listaWszystkiego;
        };




        //14** Napisz własny interfejs funkcyjny, który "konsumuje" 2 obiekty różnego typu. Zaimplementuj
        // go według własnego uznania.

        MojConsummer<String,Person> mapa=(a,b)->{
            Map<String,Person> map = new HashMap<>();
            map.put(a,b);
        };


    }


    @Override
    public void accept(Object o, Object o2) {

    }

    @Override
    public Object apply(Object o, Object o2, Object o3) {
        return null;
    }
}
