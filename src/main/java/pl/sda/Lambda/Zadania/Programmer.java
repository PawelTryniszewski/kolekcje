package pl.sda.Lambda.Zadania;

import pl.sda.Lambda.zad1predicate.Person;

import java.util.List;



public class Programmer {
    Person person;
    List<String> listaJezykow;

    @Override
    public String toString() {
        return "Programmer{" +
                "person=" + person +
                ", listaJezykow=" + listaJezykow +
                '}';
    }

    public Person getPerson() {
        return person;
    }

    public List<String> getListaJezykow() {
        return listaJezykow;
    }

    public Programmer(Person person, List<String> listaJezykow) {

        this.person = person;
        this.listaJezykow = listaJezykow;
    }
}
