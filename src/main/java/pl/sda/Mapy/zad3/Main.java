package pl.sda.Mapy.zad3;

import java.util.*;

public class Main {

    public static Map<String, Set<Integer>> metodeskorowidzLiterowy(String tekst) {
        String[] literki = tekst.split("");
        Map<String,Set<Integer>> map = new HashMap<>();
        for (int i = 0; i < literki.length; i++) {

            Set<Integer> set = new HashSet<>();
            String indeksyWystapien = null;
            int index = 0;
            for (String litera : literki){
                if (literki[i].equals(litera)){
                    set.add(index);

                }
                index++;
            }
            map.put(literki[i],set);


        }return map;
    }
    public static TreeMap<String, TreeSet<Integer>> metodeskorowidzLiterowy1(String tekst) {
        String[] literki = tekst.split("");
        TreeMap<String,TreeSet<Integer>> map = new TreeMap<>();
        for (int i = 0; i < literki.length; i++) {

            TreeSet<Integer> set = new TreeSet<>();
            String indeksyWystapien = null;
            int index = 0;
            for (String litera : literki){
                if (literki[i].equals(litera)){
                    set.add(index);

                }
                index++;
            }
            map.put(literki[i],set);


        }return map;
    }

    public static void main(String[] args) {
        String tekst="bababababababa";
        System.out.println(metodeskorowidzLiterowy(tekst));
        String hello ="Hello";
        System.out.println("Nieposortowane");
        System.out.println(metodeskorowidzLiterowy(hello));
        System.out.println("Posortowane");
        System.out.println(metodeskorowidzLiterowy1(hello));

    }
}
