package pl.sda.Listy.zad2;

import java.util.*;

/**
 * Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
 * - dodaj do listy 10 liczb losowych
 * - oblicz sumę elementów na liście (wypisz ją)
 * - oblicz średnią elementów na liście (wypisz ją)
 * - podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) - sortuje listę)
 *
 *
 *    - znajdź największy oraz najmniejszy element na liście (znajdź go posługując się pętlą for) - wypisz indeks elementu
 *     - znajdź największy oraz najmniejszy element na liście (znajdź go pętlą for, a następnie określ index
 *     posługując się metodą indexOf)
 * Sprawdź działanie aplikacji.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random r = new Random();
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        list.add(r.nextInt(100));
        System.out.println(list);
        int suma = 0;
        for (Integer i : list) {
            suma += i;
        }
        System.out.println(suma);
        int srednia = suma / list.size();
        System.out.println(srednia);
        List<Integer> listKopia = new ArrayList<>(list); // kopia nieposortowanej listy
        Collections.sort(list);
        System.out.println(list);
        double mediana=0;

        if (list.size() % 2 == 0) {

            mediana = (((list.get((list.size() / 2) - 1)) + (list.get(list.size() / 2))) / 2);

        } else
            mediana = list.get((list.size() - 1) / 2);


        System.out.println(mediana);

        double max = list.get(0);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max)
                max = list.get(i);
        }
        System.out.println("Watrosc maksymalna = " + max);
        double min = list.get(0);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < min)
                min = list.get(i);
        }
        System.out.println("Wartosc minimalna = " + min);



    }






}

