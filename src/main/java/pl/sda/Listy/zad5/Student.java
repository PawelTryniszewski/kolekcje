package pl.sda.Listy.zad5;

/**
 * Stwórz klasę Student która posiada pola:
 * nr indeksu
 * imie
 * nazwisko
 * płeć (wartość enum)
 *
 * Stwórz instancję kolekcji typu ArrayList, która zawiera obiekty klasy Student
 * w mainie dodaj do kolekcji kilku studentów (dodaj je ręcznie - wpisz cokolwiek).
 *
 * a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
 * b. Spróbuj wypisać elementy stosując pętlę foreach
 * c. Wypisz tylko kobiety
 * d. Wypisz tylko mężczyzn
 * e. Wypisz tylko indeksy osób
 */

public class Student {
    int nrIndeksu;
    String imie;
    String nazwisko;

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public Plec getPlec() {
        return plec;
    }

    public int getNrIndeksu() {

        return nrIndeksu;
    }

    @Override
    public String toString() {
        return "NrIndeksu= " + nrIndeksu +" "+ imie + " " +nazwisko +" "+plec;
    }

    Plec plec;

    public Student(int nrIndeksu, String imie, String nazwisko, Plec plec) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }
}
