package pl.sda.Listy.zad7;

public class Produkt  {

    String produktu;
    double cenaProduktu;
    PodatekProduktu podatek;


    public Produkt(String produktu, double cenaProduktu, PodatekProduktu podatek) {
        this.produktu = produktu;
        this.cenaProduktu = cenaProduktu;
        this.podatek = podatek;
    }
    public double cenaBruttoPodajVAT(PodatekProduktu podatekProduktu){
        double cenaBrutto;
        cenaBrutto=podatekProduktu.podatek*cenaProduktu+cenaProduktu;
        return cenaBrutto;
    }

    public double podajCeneBrutto(){//- która oblicza cenę brutto na podstawie ceny netto i nałożonego podatku
        double cenaBrutto;
        cenaBrutto=cenaProduktu*PodatekProduktu.VAT23.podatek+cenaProduktu;
        return cenaBrutto;


    }

    public String getProduktu() {
        return produktu;
    }

    public void setProduktu(String produktu) {
        this.produktu = produktu;
    }

    public double getCenaProduktu() {
        return cenaProduktu;
    }

    public void setCenaProduktu(double cenaProduktu) {
        this.cenaProduktu = cenaProduktu;
    }

    public PodatekProduktu getPodatek() {
        return podatek;
    }

    @Override
    public String toString() {
        return  produktu + " " +" Cena: " + cenaProduktu +
                " " + podatek;
    }

    public void setPodatek(PodatekProduktu podatek) {
        this.podatek = podatek;
    }
}
