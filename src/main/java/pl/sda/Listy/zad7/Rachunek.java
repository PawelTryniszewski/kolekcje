package pl.sda.Listy.zad7;

import java.util.ArrayList;
import java.util.List;

/**
 * Stwórz klasę Rachunek który jako pola posiada:
 * * - listę zakupionych produktów
 * *
 * * Dodaj w klasie Rachunek metody:
 * * - getter do listy zakupionych produktów
 * * - wypiszRachunek():void - metoda która wypisuje do konsoli wszystkie produkty z listy jeden pod drugim.
 * * - podsumujRachunekNetto():double - która oblicza sumę cen netto i ZWRACA tą wartość
 * * - podsumujRachunekBrutto():double - która oblicza sumę cen brutto i ZWRACA tą wartość
 * * - zwróćWartośćPodatku():double - która ZWRACA różnicę między cenami netto i brutto
 */

public class Rachunek {

    List<Produkt> listaProduktow = new ArrayList<>();

    public Rachunek(List<Produkt> listaProduktow) {
        this.listaProduktow = listaProduktow;
    }

    public double zwrocWartoscPodatku() {
        double wartoscPodatku = podsumujRachunekBrutto() - podsumujRachunekNetto();
        return wartoscPodatku;
    }
    public double wszystkieProduktyZNowymPodatkiem(PodatekProduktu podatekProduktu){
        List<Produkt> listaZNowymPodatkien = new ArrayList<>();
        for (Produkt produkt:listaProduktow){
            produkt.setPodatek(podatekProduktu);
            listaZNowymPodatkien.add(produkt);
        }
        double suma=0;
        for (Produkt produkt:listaZNowymPodatkien){
            suma += produkt.cenaBruttoPodajVAT(podatekProduktu);
        }
        return suma;
    }

    public double podsumujRachunekBrutto() {
        double sumaCenBrutto = 0;
        for (Produkt produkt : listaProduktow) {
            sumaCenBrutto += produkt.podajCeneBrutto();
        }
        return sumaCenBrutto;
    }

    public double podsumujRachunekNetto() {//która oblicza sumę cen netto i ZWRACA tą wartość
        double sumaCenNetto = 0;
        for (Produkt produkt : listaProduktow) {
            sumaCenNetto += produkt.getCenaProduktu();
        }
        return sumaCenNetto;
    }

    public List<Produkt> getListaProduktow() {
        return listaProduktow;
    }

    public void wypiszRachunek() {
        for (Produkt produkt : listaProduktow) {

            System.out.println(produkt);
        }
    }

    @Override
    public String toString() {
        return "Rachunek{" +
                "listaProduktow= " + listaProduktow +
                '}';
    }
}
