package pl.sda.Listy.zad7;

import java.util.ArrayList;
import java.util.List;

/**
 * Stworzymy koszyk zakupowy z produktami. Nasz koszyk będzie przechowywał zamówienie które możemy złożyć w
 * pizzerii lub restauracji (lub sklepie).
 *
 * Stwórz enum PodatekProduktu i dodaj wartości:
 * VAT8
 * VAT23
 * VAT5
 * NO_VAT
 *
 * Stwórz klasę Produkt. Dodaj w niej pola:
 * - nazwa produktu
 * - cena produktu (netto)
 * - ilość podatku (PodatekProduktu)
 *
 * Dodaj w klasie produkt metody:
 * - gettery oraz settery do wszystkich pól
 * - metodę podajCeneBrutto():double - która oblicza cenę brutto na podstawie ceny netto i nałożonego podatku
 *
 * Stwórz klasę Rachunek który jako pola posiada:
 * - listę zakupionych produktów
 *
 * Dodaj w klasie Rachunek metody:
 * - getter do listy zakupionych produktów
 * - wypiszRachunek():void - metoda która wypisuje do konsoli wszystkie produkty z listy jeden pod drugim.
 * - podsumujRachunekNetto():double - która oblicza sumę cen netto i ZWRACA tą wartość
 * - podsumujRachunekBrutto():double - która oblicza sumę cen brutto i ZWRACA tą wartość
 * - zwróćWartośćPodatku():double - która ZWRACA różnicę między cenami netto i brutto
 *
 * Dodatkowe:
 * ** - stwórz metodę która zwróci informację o tym ile kosztowałyby wszystkie produkty gdyby posiadały
 * podatek 8%, oraz gdyby posiadały podatek 23%.
 *
 *
 * Wskazówka: do enuma dodaj wartość liczbową reprezentującą wartość (procentowo) podatku.
 */

public class Main {
    public static void main(String[] args) {
        Produkt jablko = new Produkt("Jablko",12.1,PodatekProduktu.VAT23);
        Produkt woda = new Produkt("woda",1.1,PodatekProduktu.VAT23);
        Produkt kielbasa = new Produkt("kielbasa",2.2,PodatekProduktu.VAT23);
        Produkt mleko = new Produkt("mleko",11.2,PodatekProduktu.VAT23);
        Produkt ser = new Produkt("ser",33.3,PodatekProduktu.VAT23);
        Produkt mieso = new Produkt("mieso",21.2,PodatekProduktu.VAT23);
        List<Produkt> produkts = new ArrayList<>();
        produkts.add(jablko);
        produkts.add(woda);
        produkts.add(kielbasa);
        produkts.add(ser);
        produkts.add(mleko);
        produkts.add(mieso);
        Rachunek rachunek = new Rachunek(produkts);
//        rachunek.wypiszRachunek();
//        System.out.println("Cena netto: "+rachunek.podsumujRachunekNetto());
        System.out.println("Cena brutto: "+rachunek.podsumujRachunekBrutto());
//        System.out.println("Podatek: "+rachunek.zwrocWartoscPodatku());
//

        System.out.println(rachunek.wszystkieProduktyZNowymPodatkiem(PodatekProduktu.VAT5));
        System.out.println(rachunek.wszystkieProduktyZNowymPodatkiem(PodatekProduktu.VAT8));
        System.out.println(rachunek.wszystkieProduktyZNowymPodatkiem(PodatekProduktu.VAT23));

        System.out.println(produkts);

    }
}
