package pl.sda.Listy.zad6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Stwórz aplikację, a w niej klasę Dziennik. Stwórz również klasę Student. Klasa Student powinna:
 * - posiadać listę ocen studenta (List<Double>)
 * - posiadać (pole) numer indeksu studenta (String)
 * - posiadać (pole) imię studenta
 * - posiadać (pole) nazwisko studenta
 * <p>
 * Dziennik powinna:
 * - posiadać (jako pole) listę Studentów.
 * - posiadać metodę 'dodajStudenta(Student):void' do dodawania nowego studenta do dziennika
 * - posiadać metodę 'usuńStudenta(Student):void' do usuwania studenta
 * - posiadać metodę 'usuńStudenta(String):void' do usuwania studenta po jego numerze indexu
 * - posiadać metodę 'zwróćStudenta(String):Student' która jako parametr przyjmuje numer indexu studenta,
 * a w wyniku zwraca konkretnego studenta.
 * - posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje indeks studenta i zwraca średnią
 * ocen studenta.
 * - posiadać metodę 'podajStudentówZagrożonych():List<Student>'
 * - posiadać metodę 'posortujStudentówPoIndeksie():List<Student>' - która sortuje listę studentów po
 * numerach indeksów, a następnie zwraca posortowaną listę.
 */

public class Main {
    public static void main(String[] args) {
        List<Double> list = new ArrayList<>(Arrays.asList(1.1,2.3,4.3,5.4));
        List<Double> list1 = new ArrayList<>(Arrays.asList(1.1,2.3,4.3,5.4));
        List<Double> list2 = new ArrayList<>(Arrays.asList(1.1,2.3,4.3,5.4));
        Student pawel = new Student(12, "pp", "tt",list);
        Student z = new Student(4, "qqqq", "wwww",list1);
        Student x = new Student(5, "cxv", "vcx",list2);

        List<Student> studenci = new ArrayList<>();
        studenci.add(z);
        studenci.add(x);

        studenci.add(pawel);


        Dziennik dziennik = new Dziennik(studenci);


        System.out.println(studenci);
        dziennik.usunStudenta(pawel);
        System.out.println(studenci);
        dziennik.dodajStudenta(pawel);
        System.out.println(studenci);
        dziennik.usunStudenta("pp");
        System.out.println(studenci);
        dziennik.dodajStudenta(pawel);
        System.out.println(dziennik.zwrocStudenta(4).zwrocSrednia());
        System.out.println();
        System.out.println(dziennik.podajZagrozonych());
        System.out.println(dziennik.posortujStudentow());

    }
}
