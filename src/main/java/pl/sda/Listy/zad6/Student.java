package pl.sda.Listy.zad6;

import java.util.ArrayList;
import java.util.List;

/**
 * - posiadać listę ocen studenta (List<Double>)
 *  *     - posiadać (pole) numer indeksu studenta (String)
 *  *     - posiadać (pole) imię studenta
 *  *     - posiadać (pole) nazwisko studenta
 */

public class Student implements Comparable<Student> {
    List<Double> list = new ArrayList<>();
    int nrIndeksu;
    String imie;
    String nazwisko;

    public Student(int nrIndeksu, String imie, String nazwisko,List list) {
        this.list = list;
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }
    public double zwrocNiedostateczne(){
        double niedostateczne=0;
        for (Double d:list){
            if (d<2){
                niedostateczne++;
            }
        }
        return niedostateczne;
    }

    public double zwrocSrednia(){
        int suma = 0;
        for (Double i : list) {
            suma += i;
        }
        return suma/list.size();
    }

    public int getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public List<Double> getList() {
        return list;
    }

    public void setList(List<Double> list) {
        this.list = list;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }



    @Override
    public int compareTo(Student o) {
        if (this.getNrIndeksu()>o.getNrIndeksu()){
            return 1;

        }else if (this.getNrIndeksu()<o.getNrIndeksu()){
            return -1;
        }else
            return 0;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
