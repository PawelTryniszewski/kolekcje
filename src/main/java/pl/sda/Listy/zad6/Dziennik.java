package pl.sda.Listy.zad6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * - posiadać (jako pole) listę Studentów.
 *  *     - posiadać metodę 'dodajStudenta(Student):void' do dodawania nowego studenta do dziennika
 *  *     - posiadać metodę 'usuńStudenta(Student):void' do usuwania studenta
 *  *     - posiadać metodę 'usuńStudenta(String):void' do usuwania studenta po jego numerze indexu
 *  *       - posiadać metodę 'zwróćStudenta(String):Student' która jako parametr przyjmuje numer indexu studenta,
 *  *       a w wyniku zwraca konkretnego studenta.
 *  *     - posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje indeks studenta i zwraca średnią
 *  *     ocen studenta.
 *  *     - posiadać metodę 'podajStudentówZagrożonych():List<Student>'
 *  *     - posiadać metodę 'posortujStudentówPoIndeksie():List<Student>' - która sortuje listę studentów po
 *  *     numerach indeksów, a następnie zwraca posortowaną listę.
 */

public class Dziennik {
    List<Student> listaStudentow= new ArrayList<>();

    public List<Student> posortujStudentow(){
        Collections.sort(listaStudentow);
        return listaStudentow;
    }

    @Override
    public String toString() {
        return "Dziennik{" +
                "listaStudentow=" + listaStudentow +
                '}';
    }

    public Dziennik(List<Student> listaStudentow) {
        this.listaStudentow = listaStudentow;
    }

    public List<Student> podajZagrozonych(){
        List<Student> listaZagrozonych = new ArrayList<>();
        for (Student s:listaStudentow){
            if (s.zwrocNiedostateczne()>0){
                listaZagrozonych.add(s);

            }
        }
        return listaZagrozonych;
    }

    public double podajSrednia(int nrindeksu){
        double srednia=0;
        for (Student s:listaStudentow){
            if (s.getNrIndeksu()==nrindeksu){
                srednia=s.zwrocSrednia();
            }
        }
        return  srednia;
    }

    public void usunStudenta (Student student){
        listaStudentow.remove(student);
    }
    public void usunStudenta (String imie){
        Student o =null;
        for (Student s : listaStudentow){
            if (s.getImie().equals(imie)){

                o=s;
            }
        }
        listaStudentow.remove(o);
    }
    public Student zwrocStudenta (int nrindeksu) {
        Student szukany=null;
        for (Student s : listaStudentow) {
            if (s.getNrIndeksu()==nrindeksu) {
                szukany=s;

            }
        }
        return szukany;
    }




    public void dodajStudenta(Student student){
        listaStudentow.add(student);
    }
}
