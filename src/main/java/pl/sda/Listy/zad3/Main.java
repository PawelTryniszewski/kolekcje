package pl.sda.Listy.zad3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Stwórz oddzielnego maina, a w nim kolejną listę String'ów. Wykonaj zadania:
 * - dodaj do listy elementy Stwórz oddzielnego maina, a w nim kolejną listę String'ów. Wykonaj zadania:
 * - dodaj do listy elementy 10030, 3004, 4000, 12355, 12222, 67888, 111200, 225355, 2222, 1111, 3546, 138751, 235912 (jako stringi) (dodaj je posługując się metodą Arrays.asList())
 * <p>
 * Przykład użycia Arrays.asList():
 * ArrayList<Integer> liczby = new ArrayList<>(Arrays.asList(23, 32, 44, 55, 677, 11, 33));
 * <p>
 * - określ indeks elementu 138751 posługując się metodą indexOf
 * - sprawdź czy istnieje na liście obiekt 67888 metodą contains (wynik wypisz na ekran)
 * - sprawdź czy istnieje na liście obiekkt 67889 metodą contains (wynik wypisz na ekran)
 * - usuń z listy obiekt 67888 oraz 67889 (metoda remove)
 * - wykonaj ponownie powyższe sprawdzenia.
 * - iteruj całą listę, wypisz elementy na ekran (a. w jednej linii, b. każdy element w oddzielnej linii).
 * (jako stringi) (dodaj je posługując się metodą Arrays.asList())
 */
public class Main {
    public static void main(String[] args) {
        List<String> listaStringow = new ArrayList<>(Arrays.asList("10030", "3004", "4000",
                "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912"));

        System.out.println(listaStringow.indexOf(12355));
        System.out.println(listaStringow.contains(67888));
        listaStringow.remove((Object)67889);
        listaStringow.remove((Object)67888);

        for (String s:listaStringow){
            System.out.println(s);
        }
        for (String s:listaStringow){
            System.out.print(s+" ");
        }

    }


}
