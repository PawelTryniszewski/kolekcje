package pl.sda.Listy.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Stwórz listę Integerów. Wykonaj zadania:
 * - dodaj do listy 5 elementów ze scannera
 * - dodaj do listy 5 elementów losowych
 * - wypisz elementy
 * Sprawdź działanie aplikacji.
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random r = new Random();

        list.add(1);
        list.add(12);
        list.add(22);
        list.add(13);
        list.add(11);
        list.add(r.nextInt(10));
        list.add(r.nextInt(10));
        list.add(r.nextInt(10));
        list.add(r.nextInt(10));
        list.add(r.nextInt(10));

        System.out.println(list);
    }
}
