package pl.sda.Sety.zad3;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
 * {(1,2), (2,1), (1,1), (1,2)}.
 * Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?
 */
public class ParyLicz {

    int a;
    int b;

    @Override
    public String toString() {
        return "ParyLicz{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParyLicz para = (ParyLicz) o;
        return a == para.a &&
                b == para.b;
    }
    @Override
    public int hashCode() {

        return Objects.hash(a, b);
    }




    public ParyLicz(int a, int b) {
        this.a = a;
        this.b = b;
    }


}
