package pl.sda.Sety.zad3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ParyLicz para1 = new ParyLicz(1,2);
        ParyLicz para2 = new ParyLicz(3,6);
        ParyLicz para3 = new ParyLicz(8,6);
        ParyLicz para4 = new ParyLicz(8,6);

        Set<ParyLicz> setPar = new HashSet<>();
        setPar.add(para1);
        setPar.add(para2);
        setPar.add(para3);
        setPar.add(para4);

        System.out.println(setPar);
    }
}
