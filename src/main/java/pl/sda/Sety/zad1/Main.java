package pl.sda.Sety.zad1;

import java.util.*;

/**
 * Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
 * Wypisz liczbę elementów na ekran (metoda size())
 * Wypisz wszystkie zbioru elementy na ekran (forEach)
 * Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
 * zmień implementacje na TreeSet
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));

        System.out.println(set);
        System.out.println(set.size());

        for (Integer i :set){
            System.out.println(i);
        }

        set.remove(10);
        set.remove(12);

        System.out.println(set);
        System.out.println(set.size());

        for (Integer i :set){
            System.out.println(i);
        }

        Set<Integer> set1 = new TreeSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));

        System.out.println(set1);


    }


}
