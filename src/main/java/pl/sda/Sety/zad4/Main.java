package pl.sda.Sety.zad4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  Napisz metodę, która przyjmuje dowolną liczbę liczb typu double (varargs) i tworzy zbiór
 *  (HashSet), składający się z tych elementów, lub wyrzuca IllegalArgumentException, jeżeli
 *  elementy nie są unikalne
 * public static Set<Double> newSet(double numbers…)
 * spróbuj przerobić metodę na wersję generyczną
 * public static <T> Set<T> newSet(T... data)
 */
public class Main {

    public static Set<Double> tworzliczby(Double... doubles){

        Set<Double> zbiorDoubli = new HashSet<>(Arrays.asList(doubles));
        return zbiorDoubli;
    }

    public static <T>Set<T> tworzLiczbyGen(T... numbers){
        Set<T> zbiorDoubli1 = new HashSet<>(Arrays.asList(numbers));
        return zbiorDoubli1;
    }



    public static void main(String[] args) {
        tworzliczby(12.2,12.5,54.2,876.7,4.6);
        tworzLiczbyGen(2,1,41,23,5,2,1);
        tworzLiczbyGen("pawel","karina","gosia");
    }
}
